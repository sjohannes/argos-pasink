// Copyright 2017 Johannes Sasongko <sasongko@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define _GNU_SOURCE // Enable glibc extensions, if available

#include "util.h"

#include <stdlib.h>

#if HAVE_STRCHRNUL
#include <string.h>
#endif

// FIXME
char *util_escape_arg(const char *str) { return strdup(str); }

char *util_escape_title(const char *str, int length) {
  size_t outlength = 0;
  const unsigned char *s;
  int remaining;

  s = (const unsigned char *)str;
  remaining = length;
  while (*s && remaining) {
    outlength += (*s == '<') ? 4 : (*s == '&') ? 5 : 1;
    s++;
    if (length > 0)
      remaining--;
  }

  char *output = malloc((outlength + 1) * sizeof(char));
  if (output) {
    s = (const unsigned char *)str;
    remaining = length;
    char *o = output;
    while (*s && remaining) {
      if (*s == '<') {
        memcpy(o, "&lt;", 4 * sizeof(char));
        o += 4;
      } else if (*s == '&') {
        memcpy(o, "&amp;", 5 * sizeof(char));
        o += 5;
      } else if (*s == '|') {
        *o++ = '-';
      } else if (*s < 32 || *s == 127) { // Non-printable ASCII
        *o++ = ' ';
      } else { // Includes non-ASCII UTF-8 bytes
        *o++ = *s;
      }
      s++;
      if (length > 0)
        remaining--;
    }
    *o = '\0';
  }
  return output;
}

void util_replace_chars(char *str, const char *original,
                        const char *replacement) {
  for (; *str; str++) {
    for (const char *o = original, *r = replacement; *o && *r; o++, r++)
      *str = (*str == *o) ? *r : *str;
  }
}

char *util_strchrnul(const char *str, int ch) {
#ifdef HAVE_STRCHRNUL
  return strchrnul(str, ch);
#else
  char c = ch;
  while (*str && *str != c)
    str++;
  return (char *)str;
#endif
}

// vim: et sts=2 sw=2
