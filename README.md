# argos-pasink: Argos script to display and switch between PulseAudio sinks

argos-pasink adds a GNOME Shell menu to switch between different PulseAudio sinks.
It works as an [Argos](https://extensions.gnome.org/extension/1176/argos/) "script" (it's actually a native binary) and requires Argos to run.

To prevent overcrowding your GNOME Shell panel, argos-pasink only shows the first word of the sink description on the main button.
For example, if your default PulseAudio sink is "PCM2706 Audio Codec Analog Stereo", it will be displayed there as "PCM2706".
The dropdown menu shows the full sink descriptions, and you can show the internal sink IDs and names by holding Alt.


## Building

To build argos-pasink, you need the PulseAudio development files (`libpulse-dev` on Debian).
You can then start the build by running `make`.

By default, the makefile makes certain assumptions which should be correct for most FreeBSD- and GNU-based operating systems.
If the build fails, have a look at the `Makefile` and adjust the various flags accordingly.

To install the program, copy the `argos-pasink` executable to the `~/.config/argos/` directory as, for example, `pasink.5s.` (note the dot at the end) to make it reload every five seconds.


## Compile-time options

By default argos-pasink adds menu entries to access PulseAudio Volume Control (pavucontrol), GNOME Sound Settings, and PulseAudio Preferences (paprefs).
These can be disabled by defining `DISABLE_PAVUCONTROL`, `DISABLE_GNOME_SOUND`, and `DISABLE_PAPREFS` respectively at compile time.
For example, to disable all of them you can build using `make 'CPPFLAGS+=-DDISABLE_PAVUCONTROL -DDISABLE_GNOME_SOUND -DDISABLE_PAPREFS'`.
