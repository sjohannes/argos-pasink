// Copyright 2017 Johannes Sasongko <sasongko@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <stdbool.h>
#include <stdint.h>

/// A PulseAudio sink, which also functions as a linked list
typedef struct Sink {
  uint32_t index;
  char *name, *description;
  struct Sink *next;
} Sink;

/// Create new sink.
///
/// \a name and \a description will be copied.
Sink *sink_new(uint32_t index, const char *name, const char *description);

/// Free the current Sink and its contents, and return the next node
Sink *sink_free(Sink *sink);

/// Free all sinks in the list starting from this node
void sink_free_all(Sink *sink);

/// Create new string containing the name or description (depending on \a
/// description) of this sink, suitable for putting in an Argos string
char *sink_get_argos_label(Sink *sink, bool description);

/// Create new string containing the title (first word of description) for this
/// sink, suitable for putting in an Argos string
char *sink_get_argos_title(Sink *sink, int *len);

// vim: et sts=2 sw=2
