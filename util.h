// Copyright 2017 Johannes Sasongko <sasongko@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// Escape a string so that it can be used in an Argos attribute value safely.
///
/// FIXME: This currently only copies the string as-is.
char *util_escape_arg(const char *str);

/// Escape a string so that it can be used in an Argos menu text safely.
///
/// \a length is the maximum length of the string (in bytes, not including the
/// null terminator). Set it to -1 to use the full length of \a str.
char *util_escape_title(const char *str, int length);

/// Replace \a original chars in \a s with the corresponding \a replacement.
///
/// For example, if a character in \a s is equal to original[5], it is replaced
/// with replacement[5]. If \a original and \a replacement are of different
/// lengths, the extra characters are ignored.
void util_replace_chars(char *str, const char *original,
                        const char *replacement);

/// Return a pointer to the first occurrence of \a c (converted to byte) in \a
/// s, or to the null terminator of \a s if \a c is not present in the string.
///
/// Compatible with glibc's strchrnul.
char *util_strchrnul(const char *s, int c);

// vim: et sts=2 sw=2
