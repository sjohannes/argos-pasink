// Copyright 2017 Johannes Sasongko <sasongko@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "sink.h"
#include "util.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/// Maximum bytes in sink names & descriptions
#define MAX_SINK_NAME 256

Sink *sink_new(uint32_t index, const char *name, const char *description) {
  Sink *sink = malloc(sizeof(Sink));
  sink->index = index;
  sink->name = strndup(name, MAX_SINK_NAME - 1);
  sink->description = strndup(description, MAX_SINK_NAME - 1);
  sink->next = NULL;
  return sink;
}

Sink *sink_free(Sink *sink) {
  Sink *next = sink->next;
  free(sink->name);
  free(sink->description);
  free(sink);
  return next;
}

void sink_free_all(Sink *sink) {
  while (sink) {
    sink = sink_free(sink);
  }
}

char *sink_get_argos_label(Sink *sink, bool description) {
  char *result;
  if (description)
    result = util_escape_title(sink->description, -1);
  else
    result = util_escape_title(sink->name, -1);
  return result;
}

char *sink_get_argos_title(Sink *sink, int *len_var) {
  const char *title = sink->description;
  const char *title_end = util_strchrnul(title, ' ');
  size_t len = title_end - title;
  char *result = util_escape_title(title, len);
  if (len_var)
    *len_var = len;
  return result;
}

// vim: et sts=2 sw=2
