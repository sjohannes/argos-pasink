﻿// Copyright 2017 Johannes Sasongko <sasongko@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "sink.h"
#include "util.h"

#include <pulse/pulseaudio.h>

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

/// Maximum bytes in sink names & descriptions
#define MAX_SINK_NAME 256

/// To exit without cleaning up anything:
//#define PASINK_NOCLEANUP
#ifdef PASINK_NOCLEANUP
#define free(x)
#define pa_context_unref(x)
#define pa_mainloop_free(x)
#define pa_operation_unref(x)
#define sink_free_all(x)
#endif

// Clang is noisy about `Foo x = {0}` when we just want to default-initialize
// everything.
#ifdef __clang__
#pragma clang diagnostic ignored "-Wmissing-field-initializers"
#endif

typedef struct {
  int n_ops;
  pa_mainloop *mainloop;
  pa_context *context;
  char *default_sink_name;
  Sink *first_sink, *last_sink, *default_sink;
  const char *migrate_target;
} PulseData;

static void finish(PulseData *pa) {
  while (pa->n_ops > 0)
    pa_mainloop_iterate(pa->mainloop, 0, NULL);
}

/// Populate default_sink_name
static void get_default_sink_cb(pa_context *context, const pa_server_info *info,
                                void *data) {
  (void)context;
  PulseData *pa = data;
  pa->default_sink_name = strndup(info->default_sink_name, MAX_SINK_NAME);
  pa->n_ops--;
}

static void get_sinks_cb(pa_context *context, const pa_sink_info *info, int end,
                         void *data) {
  (void)context;
  PulseData *pa = data;
  if (end) {
    pa->n_ops--;
    return;
  }
  Sink *sink = sink_new(info->index, info->name, info->description);
  if (pa->last_sink)
    pa->last_sink->next = sink;
  else
    pa->first_sink = sink;
  pa->last_sink = sink;
  if (strncmp(sink->name, pa->default_sink_name, MAX_SINK_NAME - 1) == 0)
    pa->default_sink = sink;
}

static void generic_success_cb(pa_context *context, int success, void *data) {
  (void)context;
  (void)success;
  PulseData *pa = data;
  pa->n_ops--;
}

static void iter_sink_input_for_move_cb(pa_context *context,
                                        const pa_sink_input_info *info, int eol,
                                        void *data) {
  PulseData *pa = data;
  if (eol) {
    pa->n_ops--;
    return;
  }
  pa->n_ops++;
  pa_operation *op = pa_context_move_sink_input_by_name(
      context, info->index, pa->migrate_target, generic_success_cb, pa);
  pa_operation_unref(op);
}

void print_argos(PulseData *pa, const char *arg0) {
  pa_operation *op;

  pa->n_ops++;
  op = pa_context_get_server_info(pa->context, get_default_sink_cb, pa);
  pa_operation_unref(op);
  finish(pa);

  pa->n_ops++;
  op = pa_context_get_sink_info_list(pa->context, get_sinks_cb, pa);
  pa_operation_unref(op);
  finish(pa);

  int len;
  char *title = sink_get_argos_title(pa->default_sink, &len);
  printf("%.*s | iconName=audio-card-symbolic\n"
         "---\n",
         len, title);
  free(title);

  char *arg_arg0 = util_escape_arg(arg0);
  for (Sink *sink = pa->first_sink; sink; sink = sink->next) {
    bool is_default = (sink == pa->default_sink);
    const char *prefix = is_default ? "◾ <b>" : "◽ ";
    const char *suffix = is_default ? "</b>" : "";
    char *name = sink_get_argos_label(sink, false);
    char *description = sink_get_argos_label(sink, true);
    char *arg_name = util_escape_arg(sink->name);
    printf(
        "%s%s%s | bash=\"%s\" param1=migrate param2=\"%s\" terminal=false"
        " refresh=true\n"
        "%s%d: %s%s | bash=\"%s\" param1=migrate param2=\"%s\" terminal=false"
        " refresh=true alternate=true\n",
        prefix, description, suffix, arg_arg0, arg_name, prefix, sink->index,
        name, suffix, arg_arg0, arg_name);
    free(name);
    free(description);
    free(arg_name);
  }
  free(arg_arg0);

#if !(defined(DISABLE_PAVUCONTROL) && defined(DISABLE_GNOME_SOUND) &&          \
      defined(DISABLE_PAPREFS))
  printf("---\n"
#ifndef DISABLE_PAVUCONTROL
         "Volume Control | iconName=multimedia-volume-control bash=pavucontrol"
         " terminal=false\n"
#endif
#ifndef DISABLE_GNOME_SOUND
         "GNOME Sound Settings | iconName=preferences-system"
         " bash=gnome-control-center param1=sound terminal=false\n"
#endif
#ifndef DISABLE_PAPREFS
         "PulseAudio Preferences | iconName=preferences-desktop bash=paprefs"
         " terminal=false\n"
#endif
  );
#endif

  sink_free_all(pa->first_sink);
  free(pa->default_sink_name);
#ifndef PASINK_NOCLEANUP
  pa->default_sink_name = NULL;
  pa->first_sink = pa->last_sink = NULL;
#endif
}

void migrate(PulseData *pa, const char *target) {
  pa_operation *op;
  pa->n_ops += 2;

  op = pa_context_set_default_sink(pa->context, target, generic_success_cb, pa);
  pa_operation_unref(op);

  pa->migrate_target = target;
  op = pa_context_get_sink_input_info_list(pa->context,
                                           iter_sink_input_for_move_cb, pa);
  pa_operation_unref(op);

  finish(pa);
}

typedef enum { MODE_ARGOS, MODE_MIGRATE } Mode;

int main(int argc, char *argv[]) {
  Mode mode;
  if (argc == 1)
    mode = MODE_ARGOS;
  else if (argc == 3 && strcmp(argv[1], "migrate") == 0)
    mode = MODE_MIGRATE;
  else {
    fprintf(stderr, "Syntax: %s [migrate SINK]\n", argv[0]);
    return 0;
  }

  int retval = 0;

  const char *server = NULL;
  const char *client = "pasink";

  PulseData pa1 = {0}, *pa = &pa1;
  pa->mainloop = pa_mainloop_new();
  pa->context = pa_context_new(pa_mainloop_get_api(pa->mainloop), client);

  pa->n_ops++;
  pa_context_connect(pa->context, server, PA_CONTEXT_NOAUTOSPAWN, NULL);
  while (pa->n_ops > 0) {
    pa_mainloop_iterate(pa->mainloop, 0, NULL);
    switch (pa_context_get_state(pa->context)) {
    case PA_CONTEXT_FAILED:
      fprintf(stderr, "ERROR: Could not connect to PulseAudio.\n");
      retval = 1;
      goto cleanup;
    case PA_CONTEXT_READY:
      pa->n_ops--;
    default:;
    }
  }

  switch (mode) {
  case MODE_ARGOS:
    print_argos(pa, argv[0]);
    break;
  case MODE_MIGRATE:
    migrate(pa, argv[2]);
    break;
  }

cleanup:
  pa_context_unref(pa->context);
  pa_mainloop_free(pa->mainloop);
  return retval;
}

// vim: et sts=2 sw=2
