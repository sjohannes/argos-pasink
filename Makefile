PKG_CONFIG != command -v pkgconf || command -v pkg-config

app = argos-pasink

app_sources = argos-pasink.c sink.c util.c
app_headers = sink.h util.h

app_cflags = -std=gnu99
app_cppflags = -DHAVE_STRCHRNUL=1

deps_cppflags != $(PKG_CONFIG) --cflags libpulse
deps_ldlibs != $(PKG_CONFIG) --libs libpulse

CFLAGS = -pipe -Wall -Wextra -O2

all: $(app)

$(app): $(app_sources) $(app_headers)
	$(CC) $(app_cflags) $(CFLAGS) $(deps_cppflags) $(app_cppflags) $(CPPFLAGS) $(app_sources) $(LDFLAGS) $(deps_ldlibs) $(LDLIBS) -o $@

clean:
	-rm -f $(app)

format:
	clang-format -i $(app_sources) $(app_headers)

.PHONY: all clean format
